var host = 'http://18.140.197.136:8080/gm/constant';
// var host = 'http://localhost:8080/gm/constant';
function sendRequest(body_text){
	console.log("request",body_text);
	fetch(host, {
      method: 'POST',
      headers: {'Content-Type': 'application/json',},
      body: body_text
      }).then(response => console.log(response.json()))
		.catch((error) => {
		  console.error('Error:', error);
		});
}
function getDateFormat(date){
	console.log(date);
	var result = twoDigit(date.getDate())+"/"+twoDigit(date.getMonth()+1)+"/"+twoDigit(date.getFullYear())+" "+twoDigit(date.getHours())+":"+twoDigit(date.getMinutes())+":"+twoDigit(date.getSeconds());
	console.log(result);
	return result;
}
function twoDigit(num){
	if (num<10)
		return '0'+num;
	return num;
}